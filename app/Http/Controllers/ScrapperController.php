<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyIndustry;
use App\Industry;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Arr;

class ScrapperController extends Controller
{
    protected $siteLink=null;

    public function __construct()
    {
        $this->siteLink='http://www.mycorporateinfo.com';
    }

    public function doCompanyScrapping()
    {
        $html = file_get_contents($this->siteLink);
		echo $html;
        $crawler = new Crawler($html);
        $nodeData  = $crawler
            ->filter('body .list-group li a')
            ->each(function (Crawler $node, $i) {
                $link=$node->attr('href')??null;
                if(strpos($link, 'industry')) {
                    $industryData=['name'=>$node->text(), 'link'=>$link];
                    //Industry Data Processing
                    $industry = Industry::where('name', $industryData['name'])->first();
                    $processed=true;
                    if(empty($industry)) {
                        $processed=false;
                        $industry = new Industry;
                    }
                    $industry->name = $industryData['name'];
                    $industry->save();
                    $industryID=$industry->id;

                    //Company Data Processing
                    if(!$processed) {
                         $industryLink=$this->siteLink.$industryData['link'];
                        $this->processCompanyData($industryLink, $industryID);
                    }

                      /*
                        Pagination Processing
                        $industryDOM=file_get_contents($industryLink);
                        $industryCrawler = new Crawler($industryDOM);
                        $industryPagination  = $industryCrawler
                            ->filter('.pagination a')
                            ->each(function (Crawler $node, $i) use($industryID) {
                                $link=$node->attr('href')??null;
                                return ['name'=>$node->text(), 'link'=>$link];
                            });
                        if(!empty($industryPagination)) {
                            $linkText = Arr::pluck($industryPagination, 'name');
                            if(in_array('Next »', $linkText)) {
                                $link = Arr::pluck($industryPagination, 'link');
                                $companyListing=$this->processCompanyData($link, $industryID);
                            }
                        }
                    */
                }
            });
        dd($nodeData);
    }

    private function processCompanyData($industryLink, $industryID)
    {
        $industryDOM=file_get_contents($industryLink);
		//echo $industryLink; exit;
        $industryCrawler = new Crawler($industryDOM);
        $industryData  = $industryCrawler
            ->filter('table tbody td a')
            ->each(function (Crawler $node, $i) use($industryID) {
                $link=$node->attr('href')??null;
                $companyData=['name'=>$node->text(), 'link'=>$link];

                //Company Data Fetching and Processing
                $companyLink=$this->siteLink.$companyData['link'];
                $companyDOM=file_get_contents($companyLink);
                $companyCrawler = new Crawler($companyDOM);
                $companyDetails  = $companyCrawler
                    ->filter('div table tr td')
                    ->each(function (Crawler $node, $i) {
                        return $node->text();
                    });

                if(!empty($companyDetails)) {
                    $companyData=['cin'=>trim($companyDetails[1])??null, 'name'=>trim($companyDetails[3])??null, 'address'=>trim($companyDetails[23])??null, 'email'=>trim($companyDetails[21])??null];
                    //Company Data Storing
                    $company = Company::where('cin', $companyData['cin'])->first();
                    if(empty($company)) {
                        $company = new Company;
                    }
                    $company->cin = $companyData['cin'];
                    $company->name = $companyData['name'];
                    $company->address = $companyData['address'];
                    $company->email = $companyData['email'];
                    $company->save();
                    $companyID=$company->id;

                    //Relationship Process
                    $companyIndustry = CompanyIndustry::where('company_id', $companyID)->first();
                    if(empty($companyIndustry)) {
                        $companyIndustry = new CompanyIndustry;
                    }
                    $companyIndustry->company_id = $companyID;
                    $companyIndustry->industry_id = $industryID;
                    $companyIndustry->save();
                }
                return $companyData;
            });
        return $industryData;
    }
}
