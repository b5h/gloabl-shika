<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyIndustry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_industry';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
